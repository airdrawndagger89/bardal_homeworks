package HomeWork;

import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        int[] array = {12, 16, 66, 71, 81, 44, 24};
        ByCondition condition = number -> number % 2 == 0;
        int[] array2 = Sequence.filter(array, condition);

        condition = number -> {
            int sum = 0;
            while (number != 0) {
                sum += number % 10;
                number = number / 10;
            }
            return sum % 2 == 0;
        };

        int[]array3 = Sequence.filter(array2, condition);

        System.out.println(Arrays.toString(array2));
        System.out.println(Arrays.toString(array3));

    }
}