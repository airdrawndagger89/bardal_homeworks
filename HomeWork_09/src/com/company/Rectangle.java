package com.company;

public class Rectangle extends Figure {

    public Rectangle(double x, double y) {
        super(x, y);
    }

    public double getPerimeter() {
        return (x + y) *2;
    }
}

