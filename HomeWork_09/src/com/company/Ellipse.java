package com.company;

public class Ellipse extends Figure {
    public Ellipse(double x, double y) {
        super(x, y);
    }
    double pi = 3.1415;
    public double getPerimeter() {
        return (4*((pi*x*y)+((x-y)*(x-y)))/(x+y));
    }
}
