/*C:\Windows\system32>java -version
        java version "17.0.1" 2021-10-19 LTS
        Java(TM) SE Runtime Environment (build 17.0.1+12-LTS-39)
        Java HotSpot(TM) 64-Bit Server VM (build 17.0.1+12-LTS-39, mixed mode, sharing)

        C:\Windows\system32>javac -version
        javac 17.0.1

 */
package com.company;
import java.util.Scanner;
public class HomeWork_3 {
    public static void main(String[] args) {

        Scanner console = new Scanner(System.in);
        int a = console.nextInt();
        int b = console.nextInt();
        int c = console.nextInt();
        int locmin = 0;

        while (c != -1) {
            if (a > b && b < c) {
                locmin++;
            }
            a = b;
            b = c;
            c = console.nextInt();
        }
        System.out.println(locmin);
    }
}