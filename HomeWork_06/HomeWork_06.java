package com.company;
import java.util.Scanner;

class Main {

    public static void main(String[] args) {
        int[] array = new int[]{13, 9, 0, 16, 0, 3, 8, 0};
        Scanner console = new Scanner(System.in);
        int searchQuery = console.nextInt();

        System.out.println(getIndexOfNumber(array,searchQuery));
        getZeroesAtTheEnd(array);
    }

    public static void getZeroesAtTheEnd(int[] array) {
        int swap = 0;
        int lastIndex = array.length -1;
        for (int i = lastIndex; i >= 0; i--) {
            if (array[i] == 0) {
                array[i] = array[lastIndex - swap];
                array[lastIndex - swap] = 0;
                swap++;
            }
        }
        for (int i : array)
            System.out.print(i + " ");
    }

    public static int getIndexOfNumber(int[] array, int searchQuery) {

        for (int i = 0; i < array.length; i++) {
            int number = array[i];
            if (number == searchQuery) {
                return i;
            }
        }
        return -1;
    }
}
