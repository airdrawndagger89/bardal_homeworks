package com.company;
import java.util.Scanner;
public class HomeWork_4 {
public static void main(String[] args) {

    Scanner console = new Scanner(System.in);
    int a = console.nextInt();
    int min = 9;


        while (a != -1) {
            while (a != 0) {
                int lastDigit = a % 10;
                a = a / 10;
                if (lastDigit < min) {
                    min = lastDigit;
                }
            }
            a = console.nextInt();
        }
        System.out.println(min);
    }
}