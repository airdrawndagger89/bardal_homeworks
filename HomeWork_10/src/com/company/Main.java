package com.company;

public class Main {
    public static void main(String[] args) {

        Circle circle1 = new Circle(15, 17);
        Circle circle2 = new Circle(12, 11);
        Square square1 = new Square(14, 19);
        Square square2 = new Square(18, 20);
        Shift[] arr = new Shift[]{square1, square2, circle1, circle2};
        for (Shift shift : arr) {
            shift.getShift(10, 14);
            System.out.println(shift.toString());
        }
    }
}
