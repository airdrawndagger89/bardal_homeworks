package com.company;

public class Square extends Figure implements Shift {
    public Square(int x, int y) {
        super(x,y);
    }

    @Override
    public void getShift(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Square{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}
