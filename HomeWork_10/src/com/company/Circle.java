package com.company;

public class Circle extends Figure implements Shift {
    public Circle (int x, int y) {
        super(x,y);
    }

    @Override
    public void getShift(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "Circle{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }
}


