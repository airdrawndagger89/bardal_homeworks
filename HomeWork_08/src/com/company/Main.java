package com.company;

import java.util.Arrays;

public class Main {


    public static void main(String[] args) {
        Human human1 = new Human("Борис", 71);
        Human human2 = new Human("Антон", 115);
        Human human3 = new Human("Руслан", 57);
        Human human4 = new Human("Дмитрий", 82);
        Human human5 = new Human("Андрей", 95);
        Human human6 = new Human("Ксения", 81);
        Human human7 = new Human("Ольга", 69);
        Human human8 = new Human("Наталья", 67);
        Human human9 = new Human("Виктор", 75);
        Human human10 = new Human("Марсель", 71);
        Human[] arr = new Human[]{human1, human2, human3,human4,human5,human6,human7,human8,human9,human10};
        Arrays.sort(arr);

        for (Human human : arr) {
            System.out.println(human.toString());

        }
    }
}

