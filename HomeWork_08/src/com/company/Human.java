package com.company;

public class Human implements Comparable<Human> {
    private String name;
    private Integer weight;

    public Human(String name, int weight) {
        if (weight <= 0 || weight > 200) {
            weight = 0;
        }
        this.name = name;
        this.weight = weight;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setWeight(Integer weight) {
        if (weight <= 0 || weight > 200) {
            weight = 0;
        }
        this.weight = weight;
    }

    public Integer getWeight() {
        return this.weight;
    }

    @Override
    public String toString() {
        return
                name + ' ' + weight;
    }

    @Override
    public int compareTo(Human o) {
        return this.weight.compareTo(o.weight);
    }
}
