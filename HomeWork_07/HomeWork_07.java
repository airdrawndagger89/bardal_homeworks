package com.company;
public class Main {
    public static void main(String[] args) {
        int[] array = new int[]{8, 6, 5, 25, 7, -1, 6, 25, 8, -1, 8, 7,  25,};
        int[] arrCount = new int[201];
        int lastIndex = array.length - 1;
        int min = Integer.MAX_VALUE;
        int minIndex = Integer.MAX_VALUE;
        int max = Integer.MIN_VALUE;
        int maxIndex = Integer.MIN_VALUE;
        String minn = "Число встречающееся минимальное количество раз: ";
        String maxx = "Число встречающееся максимальное количество раз: ";

        for (int i = 0; i < lastIndex; i++) { //цикл прохода по массиву
            int a = array[i]; //записываем в а значение из первого массива
            arrCount[a + 100]++; // второй массив в ячейу а+100 (108) плюсуем 1(0+1)
        }
        for (int i = 0; i < arrCount.length; i++) { //цикл проверки на мин и макс число в индексе
            if (arrCount[i] < min && arrCount[i] > 0) {
                min = arrCount[i]; //перезаписываем минимум значением < min > 0
                minIndex = i - 100; //перезаписываем индекс в момент перезаписи минимума(-100)
            }
            if (arrCount[i] > max && arrCount[i] > 0) {
                max = arrCount[i]; //перезаписываем максимум значением
                maxIndex = i - 100; //перезаписываем индекс (-100)
            }
        }
        System.out.println(minn + minIndex);
        System.out.println(maxx + maxIndex);
    }
}